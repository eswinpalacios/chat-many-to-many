var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var users = [];

app.get('/', function(req, res){
  //res.send('<h1>Hello world</h1>');
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){

  var room = "room-a";
  var name = "";

  socket.join(room);  

	socket.on('newUser', function(txt){
    users.push(txt);
    name = txt;    

    socket.broadcast.to(room).emit('lineMessage', ">>new user " + name); //this room without user
    io.emit('listUser', users); //all rooms

    console.log("newUser: " + txt + " socket.io: " + socket.id);
  });

  socket.on('changeRoom', function(newRoom){
    socket.leave(room);
    socket.join(newRoom);
    room = newRoom;

    console.log(socket.id + " changeRoom: " + newRoom);
  });

  socket.on('chat message', function(msg){
      socket.broadcast.to(room).emit('chat message', {msg: msg, name: name}); //this room without user      
    	
      console.log(socket.id + ' | message: ' + msg + " name: " + name);
  	});
	
	socket.on('disconnect', function(){
    	var msg = ">>user "  + name + " disconnected ...";
      io.emit('lineMessage', msg); //all rooms

      removeUser(name);
      io.emit('listUser', users); //all rooms
  	});

});

function removeUser(name)
{  
  var posDelete = -1;

  for (var i = 0; i < users.length; i++) {
    if(users[i] == name)
    {
      posDelete = i;
      break;
    }
  }

  if(posDelete != -1){    
    users.splice(posDelete, 1);
    console.log("removeUser " + name);    
  }
}

http.listen(3000, function(){
  console.log('listening on *:3000');
});
